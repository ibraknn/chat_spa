package kz.aitu.chat.models;

import kz.aitu.chat.enums.MessageType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "message")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String text;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "chat_id")
    private Long chatId;

    @Column(name = "created_timestamp")
    private Long createdTimestamp;

    @Column(name = "updated_timestamp")
    private Long updatedTimestamp;

    @Column(name = "isread")
    private Boolean isRead = false;

    @Column(name = "readtimestamp")
    private Long readTimestamp;

    @Column(name = "isdelivered")
    private Boolean isDelivered = false;

    @Column(name = "deliveredtimestamp")
    private Long deliveredTimestamp;

    @Column(name = "message_type")
    private String messageType;
}
