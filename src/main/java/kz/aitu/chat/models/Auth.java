package kz.aitu.chat.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Locale;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "auth")
public class Auth {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "login")
    private String login;
    @Column(name = "password")
    private String password;
    @Column(name = "last_login_timestamp")
    private Long lastLoginTimestamp;
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "token")
    private String token = UUID.randomUUID().toString().toUpperCase();
}
