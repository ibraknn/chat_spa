package kz.aitu.chat.service;

import kz.aitu.chat.models.Auth;
import kz.aitu.chat.repositories.AuthRepository;
import kz.aitu.chat.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;


@AllArgsConstructor
@Service
public class AuthService {
    private final AuthRepository authRepository;
    private final UserRepository userRepository;

    public Auth login(Auth auth) throws Exception {
        Auth localAuth = authRepository.getAuthByLogin(auth.getLogin());
        if (!authRepository.existsByLoginAndPassword(localAuth.getLogin(), localAuth.getPassword())
                && !userRepository.existsById(localAuth.getUserId())) throw new Exception("BAD LOGIN");

        Date date = new Date();
        localAuth.setToken(UUID.randomUUID().toString().toUpperCase());
        localAuth.setLastLoginTimestamp(date.getTime());

        return authRepository.save(localAuth);
    }
}
