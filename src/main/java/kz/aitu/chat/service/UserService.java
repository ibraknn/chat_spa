package kz.aitu.chat.service;

import kz.aitu.chat.models.User;
import kz.aitu.chat.repositories.UserRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAll() {
       return this.userRepository.findAll();
    }

    public User add(User user) { return this.userRepository.save(user); }

    public User update(User user) { return this.userRepository.save(user); }

    public void delete(User user) { this.userRepository.delete(user);}

//    public List<User> getAllUsersByChatId(Long id) {
//
//    }
}
