package kz.aitu.chat.service;

import kz.aitu.chat.enums.MessageType;
import kz.aitu.chat.enums.Status;
import kz.aitu.chat.models.Message;
import kz.aitu.chat.repositories.AuthRepository;
import kz.aitu.chat.repositories.MessageRepository;
import kz.aitu.chat.repositories.ParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class MessageService {
    private MessageRepository messageRepository;
    private ParticipantRepository participantRepository;
    private AuthRepository authRepository;

    public List<Message> getAllChatById(Long chatId, Integer size) {
        return messageRepository.findByChatId(chatId, PageRequest.of(1, size, Sort.by(Sort.Direction.DESC , "createdTimestamp")).first());
    }

    public List<Message> getAllMessages() {
        return this.messageRepository.findAll();
    }

    public Message add(Message message, String token) throws Exception {
        if (message.getText().isEmpty() && message.getText().isBlank())
            throw  new Exception("Message is null");

        if (!participantRepository.existsByUserIdAndChatId(authRepository.getAuthByToken(token).getUserId(), message.getChatId()))
            throw  new Exception("Chat with this user not exist");

        if (authRepository.getAuthByToken(token).getUserId() == null || message.getChatId() == null)
            throw new Exception("userId or chatId is null");

        Date date = new Date();
        message.setCreatedTimestamp(date.getTime());
        message.setUserId(authRepository.getAuthByToken(token).getUserId());
        if (message.getMessageType().equalsIgnoreCase(MessageType.BASIC.toString())) {
            message.setMessageType(message.getText());
        }
        if (message.getMessageType().equalsIgnoreCase(MessageType.STATUS.toString())) {

        }
        return this.messageRepository.save(message);
    }

    public Message update(Message message) throws Exception {

        if(message.getId() == null)
            throw  new Exception("Message_id is null");

        if(message.getText() == null || message.getText().isEmpty())
            throw new Exception("Text is null");

        Optional<Message> checkMessage = messageRepository.findById(message.getId());
        if (checkMessage.isEmpty())
            throw new Exception("Message is not exist");

        Date date = new Date();

        message.setUpdatedTimestamp(date.getTime());

        return this.messageRepository.save(message);
    }

    public void delete(Message message) {
        this.messageRepository.delete(message);
    }

    public List<Message> getLastTenMessagesByChatId(Long id) throws Exception {
        Date date = new Date();
        List<Message> messageList = messageRepository.getLastTenMessagesByChatId(id);
        for (Message message: messageList) {
            message.setIsDelivered(true);
            message.setDeliveredTimestamp(date.getTime());
            update(message);
        }
        return messageList;
    }

    public List<Message> getLastTenMessagesByUserId(String token) {
        List<Message> messageList = messageRepository.getLastTenMessagesByUserId(authRepository.getAuthByToken(token).getUserId());
        return messageList;
    }
    public void read(String token, Long chatId) throws Exception {
        List<Message> messageList = messageRepository.getLastTenMessagesByChatId(chatId);
        Date date = new Date();
        for (Message message: messageList) {
            if (message.getUserId().equals(authRepository.getAuthByToken(token).getUserId())) {
                message.setIsRead(true);
                message.setReadTimestamp(date.getTime());

                if (message.getIsDelivered() == null) {
                    message.setIsDelivered(true);
                    message.setDeliveredTimestamp(date.getTime());
                }
                update(message);
            }
        }
    }
    public List<Message> getAllNotDelivered(Long chatId) {
        List<Message> messageList = messageRepository.findAllByIsDeliveredFalseAndChatId(chatId);
        return messageList;
    }

    public void writing(Long chatId) throws Exception {
        List<Message> messageList = messageRepository.findAllByChatId(chatId);
        for (Message message: messageList) {
            message.setMessageType(Status.WRITING.toString());
            update(message);
        }
        //SetTimeout for writing, after 4 secs it will be BASIC
        Runnable runnable = new Thread();
        new Thread(() -> {
            try {
                Thread.sleep(4000);
                runnable.run();
                for (Message message: messageList) {
                    message.setMessageType(message.getText());
                    update(message);
                }
            }
            catch (Exception e){
                System.err.println(e);
            }
        }).start();
    }
    public void online(Long chatId) throws Exception {
        List<Message> messageList = messageRepository.findAllByChatId(chatId);
        for (Message message: messageList) {
            message.setMessageType(Status.ONLINE.toString());
            update(message);
        }
        //SetTimeout for online, after 4 secs it will be BASIC
        Runnable runnable = new Thread();
        new Thread(() -> {
            try {
                runnable.run();
                for (Message message: messageList) {
                    message.setMessageType(message.getText());
                    update(message);
                }
            }
            catch (Exception e){
                System.err.println(e);
            }
        }).start();
    }
}
