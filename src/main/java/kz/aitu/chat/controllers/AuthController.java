package kz.aitu.chat.controllers;

import kz.aitu.chat.models.Auth;
import kz.aitu.chat.repositories.AuthRepository;
import kz.aitu.chat.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@Controller
@AllArgsConstructor
@RequestMapping(value = "/api/v1/auth")
public class AuthController {
    private final AuthService authService;
    private final AuthRepository authRepository;

    @PostMapping({"/login"})
    public ResponseEntity<?> login(@RequestBody Auth auth) throws Exception {
        return ResponseEntity.ok(authService.login(auth));
    }

    
}
