package kz.aitu.chat.controllers;


import kz.aitu.chat.models.Participant;
import kz.aitu.chat.service.ParticipantService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@Controller
@AllArgsConstructor
@RequestMapping(value = "/api/v1/participant")
public class ParticipantController {

    private final ParticipantService participantService;



    @GetMapping({"/chat/{id}"})
    public ResponseEntity<?> getParticipantByChatId(@PathVariable(name = "id") Long chatId) {
        return ResponseEntity.ok(participantService.getUsersByChat(chatId));
    }

    @GetMapping({"/user/{id}"})
    public ResponseEntity<?> getParticipantByUserId(@PathVariable(name = "id") Long userId) {
        return ResponseEntity.ok(participantService.getParticipantByUser(userId));
    }


}
