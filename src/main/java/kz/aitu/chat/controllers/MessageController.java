package kz.aitu.chat.controllers;

import kz.aitu.chat.models.Message;
import kz.aitu.chat.repositories.AuthRepository;
import kz.aitu.chat.repositories.MessageRepository;
import kz.aitu.chat.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@RestController
@Controller
@AllArgsConstructor
@RequestMapping(value = "/api/v1/message")
public class MessageController {
    private MessageService messageService;
    private MessageRepository messageRepository;
    private AuthRepository authRepository;


    @PostMapping({"/create"})
    public ResponseEntity<?> add(@RequestHeader("token") String token, @RequestBody Message message) throws Exception {
        Message savedMessage = messageService.add(message, token);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedMessage.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping({"/update/{id}"})
    public ResponseEntity<?> update(@RequestHeader("token") String token, @RequestBody Message message, @PathVariable Long id) throws Exception {
        Optional<Message> messageOptional = messageRepository.findById(id);
        if (!messageOptional.isPresent()) return ResponseEntity.notFound().build();
        if (messageOptional.get().getUserId() != authRepository.getAuthByToken(token).getUserId()) return ResponseEntity.badRequest().body("It is not user message!");
        message.setId(id);
        message.setCreatedTimestamp(messageOptional.get().getCreatedTimestamp());
        messageService.update(message);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping({"/delete"})
    public void delete(@RequestHeader("token") String token, @RequestBody Message message) {
        if (message.getUserId() == authRepository.getAuthByToken(token).getUserId())
            messageService.delete(message);
    }

    @GetMapping({"/getLastTenMessagesByChatId/{id}"})
    public ResponseEntity<?> getLastTenMessagesByChatId(@PathVariable Long id) throws Exception {
        return ResponseEntity.ok(messageService.getLastTenMessagesByChatId(id));
    }

    @GetMapping({"/getLastTenMessagesByUserId"})
    public ResponseEntity<?> getLastTenMessagesByUserId(@RequestHeader("token") String token) {
        return ResponseEntity.ok(messageService.getLastTenMessagesByUserId(token));
    }

    @GetMapping({"/getAll"})
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(messageService.getAllMessages());
    }

    @PutMapping({"/read/{chatId}"})
    public void read(@RequestHeader("token") String token, @PathVariable Long chatId) throws Exception {
       messageService.read(token, chatId);
    }

    @GetMapping({"/getAllNotDelivered/{chatId}"})
    public ResponseEntity<?> getAllNotDelivered(@PathVariable Long chatId) {
        return ResponseEntity.ok(messageService.getAllNotDelivered(chatId));
    }

    @GetMapping({"/status/writing/{chatId}"})
    public void writing(@PathVariable Long chatId) throws Exception {
        messageService.writing(chatId);
    }

    @GetMapping({"/status/online/{chatId}"})
    public void online(@PathVariable Long chatId) throws Exception {
        messageService.online(chatId);
    }
}
