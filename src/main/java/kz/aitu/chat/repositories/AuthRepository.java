package kz.aitu.chat.repositories;

import kz.aitu.chat.models.Auth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends JpaRepository<Auth, Long> {
    boolean existsByLoginAndPassword(String login, String password);
    Auth getAuthByToken(String token);
    Auth getAuthByLogin(String login);
}
