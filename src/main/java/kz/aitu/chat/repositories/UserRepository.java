package kz.aitu.chat.repositories;

import kz.aitu.chat.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findAll();

    User deleteUserById(Long id);

    boolean existsById(Long id);

}
