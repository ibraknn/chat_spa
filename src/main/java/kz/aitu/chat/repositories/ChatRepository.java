package kz.aitu.chat.repositories;

import kz.aitu.chat.models.Chat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChatRepository extends JpaRepository<Chat, Long> {
}
