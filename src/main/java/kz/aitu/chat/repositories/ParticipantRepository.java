package kz.aitu.chat.repositories;

import kz.aitu.chat.models.Participant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ParticipantRepository extends JpaRepository<Participant, Long> {
    List<Participant> findAllByChatId(Long chatId);
    List<Participant> findAllByUserId(Long userId);
    Boolean existsByUserIdAndChatId(Long userId,Long chatId);

}
