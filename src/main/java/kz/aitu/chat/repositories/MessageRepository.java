package kz.aitu.chat.repositories;

import kz.aitu.chat.models.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findAllByChatId(Long id);

    @Query(value = "SELECT * FROM message m WHERE m.chat_id = ?1 ORDER BY m.id DESC LIMIT 10", nativeQuery = true)
    List<Message> getLastTenMessagesByChatId(Long id);

    @Query(value = "SELECT * FROM message m WHERE m.user_id = ?1 ORDER BY m.id DESC LIMIT 10", nativeQuery = true)
    List<Message> getLastTenMessagesByUserId(Long id);

    List<Message> findByChatId(Long chatId , Pageable pageable);

    Message findByChatId(Long chatId);

    List<Message> findAllByIsDeliveredFalseAndChatId(Long chatId);

}
